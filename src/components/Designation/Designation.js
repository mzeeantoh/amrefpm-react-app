import { Grid, TableContainer, Table, TableHead, TableRow, TableCell, TableBody, withStyles, Paper, ButtonGroup, Button } from '@material-ui/core';
import React, { useState, useEffect } from 'react';
import { connect } from "react-redux";
import * as actions from "../../actions/designation/designationAction";
import DesignationForm from "./DesignationForm";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import { useToasts } from "react-toast-notifications";

const styles = theme => ({
    paper: {
        margin: theme.spacing(2),
        padding: theme.spacing(2),
    }
})

const Designation = ({ classes, ...props }) => {
    const { addToast } = useToasts()
    const [currentId, setCurrentId] = useState(0)

    useEffect(() => {
        props.fetchAllDesignations()
    }, []);

    const onDelete = id => {
        if (window.confirm('Are you sure you want to delete this record?')) {
            const onSuccess = () => {
                addToast("Submitted successfully", { appearance: 'info' })
            }
            props.deleteDesignation(id, onSuccess)
        }
    }
    return (
        <Paper className={classes.paper}>
            <h1>Designations</h1>
            <Grid container>
                <Grid item xs={6}>
                    <DesignationForm {...({ currentId, setCurrentId })} />
                </Grid>
                <Grid item xs={6}>
                    <TableContainer>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Title</TableCell>
                                    <TableCell>Description</TableCell>
                                    <TableCell></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    props.designationList.map((record, index) => {
                                        return (
                                            <TableRow key={index} hover>
                                                <TableCell>{record.title}</TableCell>
                                                <TableCell>{record.description}</TableCell>
                                                <TableCell>
                                                    <ButtonGroup>
                                                        <Button><EditIcon color="primary" onClick={() => setCurrentId(record.id)} /></Button>
                                                        <Button><DeleteIcon color="secondary" onClick={() => onDelete(record.id)} /></Button>
                                                    </ButtonGroup>
                                                </TableCell>
                                            </TableRow>
                                        )
                                    })
                                }
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
            </Grid>
        </Paper>
    );
}

const mapStateToProps = state => {
    return {
        designationList: state.designationReducer.list
    }
}

const mapActionToProps = {
    fetchAllDesignations: actions.fetchAll,
    deleteDesignation: actions.deleteDesignation
}

export default connect(mapStateToProps, mapActionToProps)(withStyles(styles)(Designation));