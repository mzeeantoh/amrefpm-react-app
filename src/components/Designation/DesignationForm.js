import React, { useState, useEffect } from "react";
import { Button, Grid, TextField, withStyles } from "@material-ui/core";
import useForm from "../Shared/useForm";
import * as actions from "../../actions/designation/designationAction";
import { connect } from "react-redux";
import { useToasts } from "react-toast-notifications";

const styles = theme => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: '200',
        },
    },

    btnMargin: {
        margin: theme.spacing(1),
    }
})

const initialFieldValues = {
    title: '',
    description: ''
}

const DesignationForm = ({ classes, ...props }) => {
    const { addToast } = useToasts()

    const validate = (fieldValues = values) => {
        let temp = { ...errors }
        if ('title' in fieldValues)
            temp.title = fieldValues.title != "" ? "" : "Please fill in the Designation"

        if ('description' in fieldValues)
            temp.description = fieldValues.description != "" ? "" : "Please fill in the description"

        setErrors({
            ...temp
        })

        if (fieldValues == values)
            return Object.values(temp).every(x => x == "")
    }

    const {
        values,
        setValues,
        errors,
        setErrors,
        handleInputChange,
        resetForm
    } = useForm(initialFieldValues, validate, props.setCurrentId)

    const handleSubmit = e => {
        e.preventDefault()

        if (validate()) {
            const onSuccess = () => {
                resetForm()
                addToast("Submitted successfully", { appearance: 'success' })
            }

            if (props.currentId == 0)
                props.createDesignation(values, onSuccess)
            else
                props.updateDesignation(props.currentId, values, onSuccess)
        }
    }

    useEffect(() => {
        if (props.currentId != 0) {
            setValues({
                ...props.designationList.find(x => x.id == props.currentId)
            });
        }
        setErrors({})
    }, [props.currentId])

    return (
        <form autoComplete="off" noValidate className={classes.root} onSubmit={handleSubmit}>
            <Grid container>
                <Grid item>

                    <TextField
                        name="title"
                        variant="outlined"
                        label="Designation"
                        value={values.title}
                        onChange={handleInputChange}
                        {...(errors.title && { error: true, helperText: errors.title })}
                    />

                    <TextField
                        name="description"
                        variant="outlined"
                        label="Description"
                        multiline
                        rowsMax={4}
                        value={values.description}
                        onChange={handleInputChange}
                        {...(errors.description && { error: true, helperText: errors.description })}
                    />
                </Grid>
            </Grid>
            <div>
                <Button className={classes.btnMargin}
                    variant="contained"
                    color="primary"
                    type="submit">Save</Button>

                <Button className={classes.btnMargin}
                    variant="contained"
                >Reset</Button>
            </div>
        </form>

    );
}


const mapStateToProps = state => {
    return {
        designationList: state.designationReducer.list
    }
}

const mapActionToProps = {
    createDesignation: actions.create,
    updateDesignation: actions.update
}

export default connect(mapStateToProps, mapActionToProps)(withStyles(styles)(DesignationForm));