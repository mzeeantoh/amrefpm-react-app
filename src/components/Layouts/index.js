import React, { useState } from 'react';
import { Layout, Menu } from 'antd';
import { UploadOutlined, UserOutlined, VideoCameraOutlined } from '@ant-design/icons';
import Kpi from "../Kpi/Kpi";
import Designation from "../Designation/Designation";
import Objective from "../Objective/Objective";
import Employee from "../Employee/Employee";
import Project from "../Project/Project";
const { SubMenu } = Menu;

const { Header, Content, Footer, Sider } = Layout;

const AppLayout = () => {
    const [page, setPage] = useState("1");

    const handlePage = ({ key }) => {
        console.log(key);
        setPage(key);
    }

    return (
        <Layout id="components-layout-demo-responsive">
            <Sider
                breakpoint="lg"
                collapsedWidth="0"
                onBreakpoint={broken => {
                    console.log(broken);
                }}
                onCollapse={(collapsed, type) => {
                    console.log(collapsed, type);
                }}
            >
                <div className="" ><h1>Amref Project Management</h1></div>
                <Menu theme="dark" mode="inline" defaultSelectedKeys={[{ page }]}>
                    <Menu.Item key="1" icon={<UserOutlined />} onClick={handlePage}>
                        Project Manager
        </Menu.Item>

                    <SubMenu key="sub1" icon={<UserOutlined />} title="Setups">
                        <Menu.Item key="2" onClick={handlePage}>KPI</Menu.Item>
                        <Menu.Item key="3" onClick={handlePage}>Designation</Menu.Item>
                        <Menu.Item key="4" onClick={handlePage}>Employees</Menu.Item>
                    </SubMenu>
                </Menu>
            </Sider>
            <Layout>
                <Header className="site-layout-sub-header-background" style={{ padding: 0 }} />
                <Content style={{ margin: '24px 16px 0' }}>
                    <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>
                        {page == "1" ? (
                            <Project setPage={setPage} />
                        ) : page == "2" ? <Kpi />
                                : page == "3" ? <Designation />
                                    : page == "4" ? <Employee /> : <Objective />}
                    </div>
                </Content>
                <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>
            </Layout>
        </Layout>
    )
}

export default AppLayout;