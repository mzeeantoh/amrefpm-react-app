import { Grid, TableContainer, Table, TableHead, TableRow, TableCell, TableBody, withStyles, Paper, ButtonGroup, Button } from '@material-ui/core';
import React, { useState, useEffect } from 'react';
import { connect } from "react-redux";
import * as actions from "../../actions/employee/employeeAction";
import EmployeeForm from "./EmployeeForm";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import { useToasts } from "react-toast-notifications";

const styles = theme => ({
    paper: {
        margin: theme.spacing(2),
        padding: theme.spacing(2),
    }
})

const Employee = ({ classes, ...props }) => {
    const { addToast } = useToasts()
    const [currentId, setCurrentId] = useState(0)

    useEffect(() => {
        props.fetchAllEmployees()
    }, []);

    const onDelete = id => {
        if (window.confirm('Are you sure you want to delete this record?')) {
            const onSuccess = () => {
                addToast("Submitted successfully", { appearance: 'info' })
            }
            props.deleteEmployee(id, onSuccess)
        }
    }
    return (
        <Paper className={classes.paper}>
            <h1>Employees</h1>
            <Grid container>
                <Grid item xs={4}>
                    <EmployeeForm {...({ currentId, setCurrentId })} />
                </Grid>
                <Grid item xs={8}>
                    <TableContainer>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Name</TableCell>
                                    <TableCell>Phone Number</TableCell>
                                    <TableCell>Email</TableCell>
                                    <TableCell>Designation</TableCell>
                                    <TableCell></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    props.employeeList.map((record, index) => {
                                        return (
                                            <TableRow key={index} hover>
                                                <TableCell>{record.firstName + record.otherName}</TableCell>
                                                <TableCell>{record.phoneNumber}</TableCell>
                                                <TableCell>{record.email}</TableCell>
                                                <TableCell>{record.designation}</TableCell>
                                                <TableCell><ButtonGroup>
                                                    <Button><EditIcon color="primary" onClick={() => setCurrentId(record.id)} /></Button>
                                                    <Button><DeleteIcon color="secondary" onClick={() => onDelete(record.id)} /></Button>
                                                </ButtonGroup>
                                                </TableCell>
                                            </TableRow>
                                        )
                                    })
                                }
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
            </Grid>
        </Paper>
    );
}

const mapStateToProps = state => {
    return {
        employeeList: state.employeeReducer.list
    }
}

const mapActionToProps = {
    fetchAllEmployees: actions.fetchAll,
    deleteEmployee: actions.deleteEmployee
}

export default connect(mapStateToProps, mapActionToProps)(withStyles(styles)(Employee));