import React, { useState, useEffect } from "react";
import { Button, Grid, TextField, FormControl, InputLabel, withStyles, Select, MenuItem, FormHelperText } from "@material-ui/core";
import useForm from "../Shared/useForm";
import * as employeeActions from "../../actions/employee/employeeAction";
import * as designationActions from "../../actions/designation/designationAction";
import { connect } from "react-redux";
import { useToasts } from "react-toast-notifications";

const styles = theme => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: 300,
        },
    },

    formControl: {
        margin: theme.spacing(1),
        minWidth: 300,
    },

    btnMargin: {
        margin: theme.spacing(1),
    }
})

const initialFieldValues = {
    firstName: '',
    otherName: '',
    phoneNumber: '',
    email: '',
    designation: '',
}

const EmployeeForm = ({ classes, ...props }) => {
    const { addToast } = useToasts()

    const validate = (fieldValues = values) => {
        let temp = { ...errors }
        if ('firstName' in fieldValues)
            temp.firstName = fieldValues.firstName != "" ? "" : "Please fill in the firstName"

        if ('otherName' in fieldValues)
            temp.otherName = fieldValues.otherName != "" ? "" : "Please fill in the otherName"

        if ('phoneNumber' in fieldValues)
            temp.otherName = fieldValues.phoneNumber != "" ? "" : "Please fill in the phoneNumber"

        if ('email' in fieldValues)
            temp.email = fieldValues.email != "" ? "" : "Please fill in the email"

        if ('designation' in fieldValues)
            temp.designation = fieldValues.designation ? "" : "This field is required."

        setErrors({
            ...temp
        })

        if (fieldValues == values)
            return Object.values(temp).every(x => x == "")
    }

    const {
        values,
        setValues,
        errors,
        setErrors,
        handleInputChange,
        resetForm
    } = useForm(initialFieldValues, validate, props.setCurrentId)

    //material-ui select
    const inputLabel = React.useRef(null);
    const [labelWidth, setLabelWidth] = React.useState(0);
    React.useEffect(() => {
        setLabelWidth(inputLabel.current.offsetWidth);
    }, []);

    const handleSubmit = e => {
        e.preventDefault()

        if (validate()) {//window.alert('validation succesded')
            const onSuccess = () => {
                resetForm()
                addToast("Submitted successfully", { appearance: 'success' })
            }

            if (props.currentId == 0)
                props.createEmployee(values, onSuccess)
            else
                props.updateEmployee(props.currentId, values, onSuccess)
        }
    }

    useEffect(() => {
        props.fetchDesignations();
        if (props.currentId != 0) {
            setValues({
                ...props.employeeList.find(x => x.id == props.currentId)
            });
        }
        setErrors({})
    }, [props.currentId])

    return (
        <form autoComplete="off" noValidate className={classes.root} onSubmit={handleSubmit}>
            <Grid container>
                <Grid item>

                    <TextField
                        name="firstName"
                        variant="outlined"
                        label="First Name"
                        value={values.firstName}
                        onChange={handleInputChange}
                        {...(errors.firstName && { error: true, helperText: errors.firstName })}
                    />

                    <TextField
                        name="otherName"
                        variant="outlined"
                        label="Last Name"
                        value={values.otherName}
                        onChange={handleInputChange}
                        {...(errors.otherName && { error: true, helperText: errors.otherName })}
                    />

                    <TextField
                        name="phoneNumber"
                        variant="outlined"
                        label="Phone Number"
                        value={values.phoneNumber}
                        onChange={handleInputChange}
                        {...(errors.phoneNumber && { error: true, helperText: errors.phoneNumber })}
                    />

                    <TextField
                        name="email"
                        variant="outlined"
                        label="Email"
                        value={values.email}
                        onChange={handleInputChange}
                        {...(errors.email && { error: true, helperText: errors.email })}
                    />

                    <FormControl variant="outlined"
                        className={classes.formControl}
                        {...(errors.designation && { error: true })}
                    >
                        <InputLabel ref={inputLabel}>Designation</InputLabel>
                        <Select
                            name="designation"
                            value={values.designation}
                            onChange={handleInputChange}
                            labelWidth={labelWidth}
                        >

                            <MenuItem value="">Select Designation</MenuItem>
                            {
                                props.designationList.map((record, index) => {
                                    return (
                                        <MenuItem value={record.id}>{record.title}</MenuItem>
                                    )
                                })
                            }
                        </Select>
                        {errors.designation && <FormHelperText>{errors.designation}</FormHelperText>}
                    </FormControl>
                </Grid>
            </Grid>
            <div>
                <Button className={classes.btnMargin}
                    variant="contained"
                    color="primary"
                    type="submit">Save</Button>

                <Button className={classes.btnMargin}
                    variant="contained"
                >Reset</Button>
            </div>
        </form>

    );
}


const mapStateToProps = state => {
    return {
        employeeList: state.employeeReducer.list,
        designationList: state.designationReducer.list
    }
}

const mapActionToProps = {
    createEmployee: employeeActions.create,
    updateEmployee: employeeActions.update,
    fetchDesignations: designationActions.fetchAll
}

export default connect(mapStateToProps, mapActionToProps)(withStyles(styles)(EmployeeForm));