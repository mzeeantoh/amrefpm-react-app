import { Grid, TableContainer, Table, TableHead, TableRow, TableCell, TableBody, withStyles, Paper, ButtonGroup, Button, Link } from '@material-ui/core';
import React, { useState, useEffect } from 'react';
import { connect } from "react-redux";
import * as actions from "../../actions/project/projectAction";
import ProjectForm from "./ProjectForm";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import { useToasts } from "react-toast-notifications";

const styles = theme => ({
    paper: {
        margin: theme.spacing(2),
        padding: theme.spacing(2),
    }
})

const Project = ({ setPage, classes, ...props }) => {
    const { addToast } = useToasts()
    const [currentId, setCurrentId] = useState(0)

    useEffect(() => {
        props.fetchAllProjects()
    }, []);

    const onDelete = id => {
        if (window.confirm('Are you sure you want to delete this record?')) {
            const onSuccess = () => {
                addToast("Submitted successfully", { appearance: 'info' })
            }
            props.deleteProject(id, onSuccess)
        }
    }
    return (
        <Paper className={classes.paper}>
            <h1>Projects</h1>
            <Grid container>
                <Grid item xs={4}>
                    <ProjectForm {...({ currentId, setCurrentId })} />
                </Grid>
                <Grid item xs={8}>
                    <TableContainer>
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Project</TableCell>
                                    <TableCell>Budget</TableCell>
                                    <TableCell>Country</TableCell>
                                    <TableCell>Project Manager</TableCell>
                                    <TableCell></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {
                                    props.projectList.map((record, index) => {
                                        return (
                                            <TableRow key={index} hover>
                                                <TableCell>{record.title}</TableCell>
                                                <TableCell>{record.budget}</TableCell>
                                                <TableCell>{record.country}</TableCell>
                                                <TableCell>{record.projectManager}</TableCell>
                                                <TableCell>
                                                    <div>
                                                        <a onClick={() => setPage("5")}>View Objectives  </a>
                                                        <ButtonGroup>
                                                            <Button><EditIcon color="primary" onClick={() => setCurrentId(record.id)} /></Button>
                                                            <Button><DeleteIcon color="secondary" onClick={() => onDelete(record.id)} /></Button>
                                                        </ButtonGroup>
                                                    </div>
                                                </TableCell>
                                            </TableRow>
                                        )
                                    })
                                }
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
            </Grid>
        </Paper>
    );
}

const mapStateToProps = state => {
    return {
        projectList: state.projectReducer.list
    }
}

const mapActionToProps = {
    fetchAllProjects: actions.fetchAll,
    deleteProject: actions.deleteProject
}

export default connect(mapStateToProps, mapActionToProps)(withStyles(styles)(Project));