import React, { useState, useEffect } from "react";
import { Button, Grid, TextField, FormControl, InputLabel, withStyles, Select, MenuItem, FormHelperText, TableContainer, Table, TableHead, TableRow, TableCell, TableBody } from "@material-ui/core";
import useForm from "../Shared/useForm";
import * as actions from "../../actions/project/projectAction";
import * as employeeActions from "../../actions/employee/employeeAction";
import { connect } from "react-redux";
import { useToasts } from "react-toast-notifications";

const styles = theme => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: 300,
        },
    },

    formControl: {
        margin: theme.spacing(1),
        minWidth: 300,
    },

    btnMargin: {
        margin: theme.spacing(1),
    }
})

const initialFieldValues = {
    title: '',
    budget: '',
    country: '',
    projectManager: ''
}

const ProjectForm = ({ classes, ...props }) => {
    const { addToast } = useToasts()

    const validate = (fieldValues = values) => {
        let temp = { ...errors }
        if ('title' in fieldValues)
            temp.title = fieldValues.title != "" ? "" : "Please fill in the title"

        if ('budget' in fieldValues)
            temp.budget = fieldValues.budget != "" ? "" : "Please fill in the budget"

        if ('country' in fieldValues)
            temp.country = fieldValues.country != "" ? "" : "Please fill in the country"


        if ('projectManager' in fieldValues)
            temp.projectManager = fieldValues.projectManager ? "" : "This field is required."

        setErrors({
            ...temp
        })

        if (fieldValues == values)
            return Object.values(temp).every(x => x == "")
    }

    const {
        values,
        setValues,
        errors,
        setErrors,
        handleInputChange,
        resetForm
    } = useForm(initialFieldValues, validate, props.setCurrentId)

    //material-ui select
    const inputLabel = React.useRef(null);
    const [labelWidth, setLabelWidth] = React.useState(0);
    React.useEffect(() => {
        setLabelWidth(inputLabel.current.offsetWidth);
    }, []);

    const handleSubmit = e => {
        e.preventDefault()

        if (validate()) {//window.alert('validation succesded')
            const onSuccess = () => {
                resetForm()
                addToast("Submitted successfully", { appearance: 'success' })
            }

            if (props.currentId == 0)
                props.createProject(values, onSuccess)
            else
                props.updateProject(props.currentId, values, onSuccess)
        }
    }

    useEffect(() => {
        props.fetchProjectManagers();
        if (props.currentId != 0) {
            setValues({
                ...props.projectList.find(x => x.id == props.currentId)
            });
        }
        setErrors({})
    }, [props.currentId])

    return (

        <form autoComplete="off" noValidate className={classes.root} onSubmit={handleSubmit}>
            <Grid container>
                <Grid item>
                    <TextField
                        name="title"
                        variant="outlined"
                        label="Project Name"
                        value={values.title}
                        onChange={handleInputChange}
                        {...(errors.title && { error: true, helperText: errors.title })}
                    />

                    <TextField
                        name="budget"
                        variant="outlined"
                        label="Budget"
                        value={values.budget}
                        onChange={handleInputChange}
                        {...(errors.budget && { error: true, helperText: errors.budget })}
                    />

                    <TextField
                        name="country"
                        variant="outlined"
                        label="Country"
                        value={values.country}
                        onChange={handleInputChange}
                        {...(errors.country && { error: true, helperText: errors.country })}
                    />


                    <FormControl variant="outlined"
                        className={classes.formControl}
                        {...(errors.projectManager && { error: true })}
                    >
                        <InputLabel ref={inputLabel}>Project Manager</InputLabel>
                        <Select
                            name="projectManager"
                            value={values.projectManager}
                            onChange={handleInputChange}
                            labelWidth={labelWidth}
                        >
                            <MenuItem value="">Select Project Manager</MenuItem>
                            {
                                props.projectManagerList.map((record, index) => {
                                    return (
                                        <MenuItem value={record.id}>{record.firstName + ' ' + record.otherName}</MenuItem>
                                    )
                                })
                            }
                        </Select>
                        {errors.projectManager && <FormHelperText>{errors.projectManager}</FormHelperText>}
                    </FormControl>
                </Grid>
            </Grid>
            <div>
                <Button className={classes.btnMargin}
                    variant="contained"
                    color="primary"
                    type="submit">Save</Button>

                <Button className={classes.btnMargin}
                    variant="contained"
                >Reset</Button>
            </div>
        </form>



    );
}


const mapStateToProps = state => {
    return {
        projectList: state.projectReducer.list,
        projectManagerList: state.employeeReducer.projectManagers
    }
}

const mapActionToProps = {
    createProject: actions.create,
    updateProject: actions.update,
    fetchProjectManagers: employeeActions.fetchProjectManagers
}

export default connect(mapStateToProps, mapActionToProps)(withStyles(styles)(ProjectForm));