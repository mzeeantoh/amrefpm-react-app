import React, { useState, useEffect } from "react";
import { Button, Grid, TextField, FormControl, InputLabel, withStyles, Select, MenuItem, FormHelperText } from "@material-ui/core";
import useForm from "../Shared/useForm";
import * as actions from "../../actions/objective/objectiveAction";
import { connect } from "react-redux";
import { useToasts } from "react-toast-notifications";

const styles = theme => ({
    root: {
        '& .MuiTextField-root': {
            margin: theme.spacing(1),
            width: 300,
        },
    },

    formControl: {
        margin: theme.spacing(1),
        minWidth: 300,
    },

    btnMargin: {
        margin: theme.spacing(1),
    }
})

const initialFieldValues = {
    title: '',
    description: '',
    kpi: ''
}

const ObjectiveForm = ({ classes, ...props }) => {
    const { addToast } = useToasts()

    const validate = (fieldValues = values) => {
        let temp = { ...errors }
        if ('title' in fieldValues)
            temp.title = fieldValues.title != "" ? "" : "Please fill in the title"

        if ('description' in fieldValues)
            temp.description = fieldValues.description != "" ? "" : "Please fill in the description"

        if ('kpi' in fieldValues)
            temp.kpi = fieldValues.kpi ? "" : "This field is required."

        setErrors({
            ...temp
        })

        if (fieldValues == values)
            return Object.values(temp).every(x => x == "")
    }

    const {
        values,
        setValues,
        errors,
        setErrors,
        handleInputChange,
        resetForm
    } = useForm(initialFieldValues, validate, props.setCurrentId)

    //material-ui select
    const inputLabel = React.useRef(null);
    const [labelWidth, setLabelWidth] = React.useState(0);
    React.useEffect(() => {
        setLabelWidth(inputLabel.current.offsetWidth);
    }, []);

    const handleSubmit = e => {
        e.preventDefault()

        if (validate()) {//window.alert('validation succesded')
            const onSuccess = () => {
                resetForm()
                addToast("Submitted successfully", { appearance: 'success' })
            }

            if (props.currentId == 0)
                props.createObjective(values, onSuccess)
            else
                props.updateObjective(props.currentId, values, onSuccess)
        }
    }

    useEffect(() => {
        if (props.currentId != 0) {
            setValues({
                ...props.objectiveList.find(x => x.id == props.currentId)
            });
        }
        setErrors({})
    }, [props.currentId])

    return (
        <form autoComplete="off" noValidate className={classes.root} onSubmit={handleSubmit}>
            <Grid container>
                <Grid item>

                    <TextField
                        name="title"
                        variant="outlined"
                        label="Objective"
                        value={values.title}
                        onChange={handleInputChange}
                        {...(errors.title && { error: true, helperText: errors.title })}
                    />

                    <TextField
                        name="description"
                        variant="outlined"
                        label="Descriptiodn"
                        multiline
                        rowsMax={4}
                        value={values.description}
                        onChange={handleInputChange}
                        {...(errors.description && { error: true, helperText: errors.description })}
                    />

                    <FormControl variant="outlined"
                        className={classes.formControl}
                        {...(errors.kpi && { error: true })}
                    >
                        <InputLabel ref={inputLabel}>KPI</InputLabel>
                        <Select
                            name="kpi"
                            value={values.kpi}
                            onChange={handleInputChange}
                            labelWidth={labelWidth}
                        >
                            <MenuItem value="">Select KPI</MenuItem>
                            <MenuItem value="1">Operational Cash Flow</MenuItem>
                            <MenuItem value="3">Inventory Turnovers</MenuItem>
                        </Select>
                        {errors.kpi && <FormHelperText>{errors.kpi}</FormHelperText>}
                    </FormControl>
                </Grid>
            </Grid>
            <div>
                <Button className={classes.btnMargin}
                    variant="contained"
                    color="primary"
                    type="submit">Save</Button>

                <Button className={classes.btnMargin}
                    variant="contained"
                >Reset</Button>
            </div>
        </form>

    );
}


const mapStateToProps = state => {
    return {
        objectiveList: state.objectiveReducer.list
    }
}

const mapActionToProps = {
    createObjective: actions.create,
    updateObjective: actions.update
}

export default connect(mapStateToProps, mapActionToProps)(withStyles(styles)(ObjectiveForm));