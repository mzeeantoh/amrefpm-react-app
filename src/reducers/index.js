import { combineReducers } from "redux";
import kpiReducer from "./kpiReducer";
import designationReducer from "./designationReducer";
import objectiveReducer from "./objectiveReducer";
import employeeReducer from "./employeeReducer";
import projectReducer from "./projectReducer";


const reducers = combineReducers({
    kpiReducer,
    designationReducer,
    objectiveReducer,
    employeeReducer,
    projectReducer
});

export default reducers;