import api from '../api';

export const ACTION_TYPES = {
    CREATE: 'CREATE',
    UPDATE: 'UPDATE',
    DELETE: 'DELETE',
    FETCH_ALL: 'FETCH_ALL',
    FETCH_PROJECT_MANAGERS: 'FETCH_PROJECT_MANAGERS'
}

const formatData = data => ({
    ...data,
    // status: 1,
    // kpi: parseInt(data.kpi ? data.kpi : 0)
    // title: data.title,
    // description: data.description
})

export const fetchAll = () => dispatch => {
    api.employee().fetchAll()
        .then(response => {
            console.log(response)

            dispatch({
                type: ACTION_TYPES.FETCH_ALL,
                payload: response.data
            })
        })
        .catch(err => console.log(err))

}

export const create = (data, onSuccess) => dispatch => {
    data = formatData(data);
    api.employee().create(data)
        .then(response => {
            dispatch({
                type: ACTION_TYPES.CREATE,
                payload: response.data
            })
            onSuccess()
        })
        .catch(err => console.log(err))
}

export const update = (id, data, onSuccess) => dispatch => {
    data = formatData(data);
    api.employee().update(id, data)
        .then(response => {
            dispatch({
                type: ACTION_TYPES.UPDATE,
                payload: { id: id, ...data }
            })
            onSuccess()
        })
        .catch(err => console.log(err))
}

export const deleteEmployee = (id, onSuccess) => dispatch => {
    api.employee().delete(id)
        .then(response => {
            dispatch({
                type: ACTION_TYPES.DELETE,
                payload: id
            })
            onSuccess()
        })
        .catch(err => console.log(err))
}


export const fetchProjectManagers = (id, onSuccess) => dispatch => {
    api.employee().fetchProjectManagers()
        .then(response => {
            dispatch({
                type: ACTION_TYPES.FETCH_PROJECT_MANAGERS,
                payload: response.data
            })
        })
        .catch(err => console.log(err))
}