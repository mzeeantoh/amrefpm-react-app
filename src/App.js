import logo from './logo.svg';
import './App.css';
import { store } from './actions/store'
import { Provider } from 'react-redux'
import Kpi from "./components/Kpi/Kpi";
import Designation from "./components/Designation/Designation";
import Objective from "./components/Objective/Objective";
import Employee from "./components/Employee/Employee";
import { Container } from "@material-ui/core";
import { ToastProvider } from "react-toast-notifications";
import './index.css';
import Layout from './components/Layouts';
import "antd/dist/antd.css";

function App() {
  return (
    <Provider store={store}>
      <ToastProvider autoDismiss={true}>
        {/* <Container maxWidth="lg"> */}
        <Layout />
        {/* </Container> */}
      </ToastProvider>
    </Provider >
  );
}

export default App;
